package Exercise;

public class MemberWithAttendance extends Member {
    private String attendance;

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public MemberWithAttendance(String name, String Id, String attendance){
        super( name,Id);
        this.attendance=attendance;

    }
    @Override
    public String toString(){
        return  super.toString()+ "!*!"+" Attandance Status:"+attendance;
    }
}
