package Exercise;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class MemberInformationMaintainer {

    private ArrayList<Member> members;
    private ArrayList<MemberWithAttendance> membersAttendance;
    private ArrayList<AttendanceDateWithMemberAttendance> attendanceDateWithMemberAttendances;
    Path file = Paths.get("monday.json");


    public MemberInformationMaintainer() {
        members = new ArrayList<>();
        membersAttendance= new ArrayList<>();
        attendanceDateWithMemberAttendances= new ArrayList<>();
    }


    public void readAllTheMemberWithAttendence() {
        members.clear();
        membersAttendance.clear();
        attendanceDateWithMemberAttendances.clear();
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();

        try {
            jsonObject = (JSONObject) parser.parse(new FileReader("monday.json"));
            jsonArray = (JSONArray) jsonObject.get("date");

        } catch (NullPointerException e) {
            System.out.println("The file is empty" + e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            JSONArray jsonArray2 = new JSONArray();

            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject dateInformation = (JSONObject) jsonArray.get(i);
                 Object jsonArray1 =  dateInformation.get("day");
                 jsonArray2 = (JSONArray) dateInformation.get("member");
                 membersAttendance.clear();

                for (int j = 0; j < jsonArray2.size(); j++) {
                    JSONObject memberInformation = (JSONObject) jsonArray2.get(j);
                    ArrayList<String> everyMemberNameAndID = new ArrayList<>(memberInformation.values());
                    membersAttendance.add(new MemberWithAttendance(everyMemberNameAndID.get(0), everyMemberNameAndID.get(1),
                            everyMemberNameAndID.get(2)));

                }
                attendanceDateWithMemberAttendances.add(new AttendanceDateWithMemberAttendance(jsonArray1.toString(), membersAttendance));
            }
        }
    }

    public void printAllTheMemberWithAttendence(){
        members.clear();
        membersAttendance.clear();
        attendanceDateWithMemberAttendances.clear();
        readAllTheMemberWithAttendence();
        attendanceDateWithMemberAttendances.forEach(System.out::println);
    }



    public void readAllTheMembers(){

        members.clear();
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        try {
            jsonObject = (JSONObject) parser.parse(new FileReader("example-member-list.json"));
            jsonArray = (JSONArray) jsonObject.get("members");

        } catch (NullPointerException e) {
            System.out.println("The file is empty" + e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject memberInformation = (JSONObject) jsonArray.get(i);
                ArrayList<String> everyMemberNameAndID = new ArrayList<>(memberInformation.values());
                members.add(new Member(everyMemberNameAndID.get(0), everyMemberNameAndID.get(1)));
            }
        }
    }





    public void addAttandance()throws Exception{
        members.clear();
        membersAttendance.clear();
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        readAllTheMembers();
        //readAllTheMemberWithAttendence();
        Scanner scanner= new Scanner(System.in);
        JSONObject attandanceObject= new JSONObject();
        JSONArray attandanceArray = new JSONArray();
        JSONArray memberArray = new JSONArray();

        System.out.println("Please provide the Attandance Date:(YYYY-MM-DD)");
        String date=scanner.next();
        String intArray[]= new String[members.size()];
        System.out.println("Please provide the Attandance '1' for present and '0' for absent");
        for(int i=0;i<members.size();i++){
            System.out.println("Please provide the Attandance:"+members.get(i).getMemberName());
             intArray[i]=scanner.next();
            JSONObject memberObject= new JSONObject();

            memberObject.put("name",members.get(i).getMemberName());
            memberObject.put("ID",members.get(i).getMemberID());
            memberObject.put("attandanceStatus",intArray[i]);
            memberArray.add(memberObject);

        }
        attandanceObject.put("day",date);
        attandanceObject.put("member",memberArray);

        try {
            jsonObject = (JSONObject) parser.parse(new FileReader("monday.json"));
            jsonArray = (JSONArray) jsonObject.get("date");

        } catch (NullPointerException e) {
            System.out.println("The file is empty" + e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {


            jsonArray.add(attandanceObject);
            JSONObject wraperObject = new JSONObject();
            wraperObject.put("date", jsonArray);
            ArrayList<String> list = new ArrayList<>();
            list.add(wraperObject.toJSONString());
            // to write on one.json file
            Files.write(file, list, StandardOpenOption.CREATE);
            System.out.println("Task is added succesfully");
        }
    }

    public void showAllTheMembers(){
        members.clear();
        readAllTheMembers();
        members.forEach(System.out::println);
    }

    public void showIfAnyIdIsNotEnique(){
        members.clear();
        readAllTheMembers();
        members.stream()
                .collect(Collectors.groupingBy(Member::getMemberID))
                .forEach((id, peopleWithSameId) -> {
                    if (peopleWithSameId.size() > 1) {
                        System.out.printf("People with identical ID %s are : %s%n", id, peopleWithSameId);
                    }
                });
    }

}