package Exercise;

import java.util.ArrayList;

public class AttendanceDateWithMemberAttendance {
    private String date;
    private ArrayList<MemberWithAttendance>memberWithAttendances;


    public AttendanceDateWithMemberAttendance(String date, ArrayList<MemberWithAttendance> memberWithAttendances) {
        this.date = date;
        this.memberWithAttendances = memberWithAttendances;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<MemberWithAttendance> getMemberWithAttendances() {
        return memberWithAttendances;
    }

    public void setMemberWithAttendances(ArrayList<MemberWithAttendance> memberWithAttendances) {
        this.memberWithAttendances = memberWithAttendances;
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        String atttendanceDate = "Date of The Attandence:"+ date;
        builder.append(atttendanceDate);
        builder.append("\n*************************************'\n");

        for (MemberWithAttendance memberWithAttendance : memberWithAttendances) {
            builder.append("\n"+memberWithAttendance.toString());
        }

        return builder.toString();

    }




}
