package Exercise;

import java.util.Scanner;

public class Main {

    public static void main(String[]args)throws Exception{
        MemberInformationMaintainer memberInformationMaintainer = new MemberInformationMaintainer();
        //memberInformationMaintainer.addAttandance();
        Scanner scanner=new Scanner(System.in);
        System.out.println(">> ******************************************************");
        System.out.println(">> *****************     WELCOME    *********************");
        System.out.println(">> ******************************************************");

        boolean found=true;
        while (found) {
            System.out.println(">> Do you want to Continue:\n"+
                    ">> press 'y' to yes and press 'n' to no\n");
            char continueOption = scanner.next().charAt(0);
            if(continueOption=='y') {

                System.out.println(">> Pick an option:\n" +
                        ">> (1) Show Member List\n" +
                        ">> (2) Take attendance for Every Member for Use provided Date and Save in a Attendance File\n" +
                        ">> (3) Show Member Attandance File\n" +
                        ">> (4) Save and Quit");
                //Scanner scanner=new Scanner(System.in);
                int choseOption = scanner.nextInt();

                if (choseOption == 1) {
                    System.out.println(" Your List:");
                    System.out.println(" **********************");
                    memberInformationMaintainer.showIfAnyIdIsNotEnique();
                    memberInformationMaintainer.showAllTheMembers();
                    found=true;

                } else if (choseOption == 2) {
                    memberInformationMaintainer.addAttandance();
                    found=true;
                } else if (choseOption == 3) {
                    memberInformationMaintainer.printAllTheMemberWithAttendence();
                    found=true;
                }
                else if (choseOption == 4) {
                    found=false;
                }
                else {
                    System.out.println("You need to enter a valid key!");
                    found = true;
                }
            }
            else if (continueOption=='n') {
                found = false;
                break;

            }
            else {
                System.out.println("You need to enter a valid key!");
                found = true;
            }
        }

        System.out.println("*********************************************************************");
        System.out.println("Thank you for use the Application");
        System.out.println("*********************************************************************");
        scanner.close();
        System.exit(0);

    }

}
